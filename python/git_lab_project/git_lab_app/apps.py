from django.apps import AppConfig


class GitLabAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'git_lab_app'
